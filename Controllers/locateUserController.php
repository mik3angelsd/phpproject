<?php
	include('UsersController.php');

	$control = new UsersController();
	$users = $control->getUsers();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Carga de estilos -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<title>Listado de usuarios</title>
</head>
<body  class="text-bg-light">
	<div class="m-5">
	    <h1>Listado de usuarios</h1>
	    <table>
	    	<thead>
	    		<th>ID</th>
	    		<th>Nombre</th>
	    		<th></th>
	    	</thead>
	    	<tbody>
	    		<?php foreach ($users as $value):?>
			    	<tr class="pt-1">
			    		<td><?= $value->id ?></td>
			    		<td><?= $value->name ?></td>
			    		<td>
			    			<form action="Views/detailView.php" method="post">
			    				<input type="hidden" name="userId" value="<?= $value->id ?>">
			    				<input type="submit" name="" value="Ver detalles">
			    			</form>
			    		</td>
			    	</tr>
			    <?php endforeach;?>
	    	</tbody>
	    </table>
	</div>
</body>
</html>
	