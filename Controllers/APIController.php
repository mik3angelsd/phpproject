<?php

class APIController
{
	//Rutas para especificar la consulta de la API
	public string $routeUsers = 'https://jsonplaceholder.typicode.com/users';
	public string $routeInfoUser = 'https://jsonplaceholder.typicode.com/users/';
	public string $routeUserPosts = 'https://jsonplaceholder.typicode.com/users/';
	public string $routeUserComments = 'https://jsonplaceholder.typicode.com/post/';
	public string $routeUserAll = 'https://jsonplaceholder.typicode.com/users/';
	public string $routeSendTask = 'https://jsonplaceholder.typicode.com/todos';
	

	/**
	 * Método para obtener el listado de los usuarios
	 **/
	public function conectionApi()
	{
		try {
			$curl = file_get_contents($this->routeUsers);
			return json_decode($curl);	
		} catch (Exception $e) {
			return 'Error al obtener a los usuarios';	
		}

	}

	/**
	 * Método para obtener los detalles del usuario a traves de un id
	 * @param string $userId Id del usuario a consultar
	 */
	public function getDetailUser(string $userId)
	{
		try {
			$detailInfo = file_get_contents($this->routeInfoUser.$userId);
			return json_decode($detailInfo);
		} catch (Exception $e) {
			return "Error al obtener los detalles del usuario";
		}
	}

	/**
	 * Método para obtener el post de los usuarios mediante un id
	 * @param string $userId Id del usuario a consultar
	 * @param string $path Ruta adicional para consultar la API
	 */
	public function getPostUser(string $userId,string $path = '/posts')
	{
		try {
			$postInfo = file_get_contents($this->routeUserPosts.$userId.$path);
			return json_decode($postInfo);
		} catch (Exception $e) {
			return 'Error al obtener los post del usuario';
		}
	}

	/**
	 * Método para obtener las tareas del usuario mediante el id
	 * @param string $userId Id del usuario a consultar
	 * @param string $path Ruta adicional para consultar la API
	 */
	public function getTaskUser(string $userId,string $path = '/todos')
	{
		try {
			$postInfo = json_decode(file_get_contents($this->routeUserAll.$userId.$path));
			return $postInfo;
		} catch (Exception $e) {
			
		}
	}

	/**
	 * Método para mandar una tarea meediante un id
	 * @param string $title Título de la tarea
	 * @param string $userId Id del usuario perteneciente a la tarea
	 * @param bool $completed Estado de la tarea completa o incompleta
	 **/
	public function sendTaskUser(string $title = null,string $userId = null,bool $completed = null)
	{
		try {
			$curl = curl_init($this->routeSendTask);
			$content = json_encode([
				'title' => $title,
				'userId' => $userId,
				'completed' => $completed
			]);
			

			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json"));
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

			$json_response = curl_exec($curl);

			$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

			return ($status != 201)? false : true;
		} catch (Exception $e) {
			return 'Ocurrio un error al tratar de mandar la tarea';
		}
	}

	/**
	 * Método para obtener los comentartios del usuario
	 * @param string $postId Id del post relacionado a los comentarios
	 * @param string $path Ruta adicional para consultar la API
	 **/
	public function getCommentsUser(string $postId,string $path = '/comments')
	{
		try {
			$commentList = file_get_contents($this->routeUserComments.$postId.$path);
			return json_decode($commentList);
		} catch (Exception $e) {
			
		}
	}
}