<?php

include('APIController.php');

class UsersController
{
	public $userDetail = false;

	/**
	 * Método para obtener el listado de los usuarios
	 */
	public function getUsers()
	{
		try {
			$apiInfo = new APIController();
			$usersList = $apiInfo->conectionApi();
			return $usersList;
		} catch (Exception $e) {
			echo 'Error para obtener los usuarios registrados';	
		}
	}

	/**
	 * Método para obtener los detalles de un usuario
	 * @param string $userId Id del usuario para obteer los detalles
	 **/
	public function getDetailUser(string $userId)
	{
		try {
			$apiInfo = new APIController();
			$usersDetails = $apiInfo->getDetailUser($userId);
			return $usersDetails;
		} catch (Exception $e) {
			echo "Error para obtener los detalles del usuario";
		}
	}

	/**
	 * Método para obtener los post a tarves del ID
	 * @param string $userId Id del usuario a constular
	 **/
	public function getPosts(string $userId)
	{
		try {
			$apiInfo = new APIController();
			$postUser = $apiInfo->getPostUser($userId);
			return $postUser;
		} catch (Exception $e) {
			echo 'Error para obtener la lista de post';
		}
	}

	/**
	 * Método para obtener las tareas de un usuario
	 * @param string $userId Id del usuario a consultar
	 **/
	public function getTasks(string $userId)
	{
		try {
			$apiInfo = new APIController();
			$taskUser = $apiInfo->getTaskUser($userId);
			return $taskUser;
		} catch (Exception $e) {
			return 'Ocurrio un error al tratar de obtener las tareas';
		}
	}

	/**
	 * Método para mandar el registro de una tarea de un usuario
	 * @param string $title Título de la tarea a registrar
	 * @param string $userId Id del usuario que se le asignara la tarea
	 * @param bool $completed Status de la tarea ya sea completa o incompleta
	 */
	public function sendTask(string $title = null,string $userId = null,bool $completed = null)
	{
		try {
			$apiInfo = new APIController();
			$confirm = $apiInfo->sendTaskUser($title,$userId,$completed);
			return $confirm;
		} catch (Exception $e) {
			return false;
		}
	}
}