<?php



class CommentsController
{
	/**
	 * Método para obtener los comentarios de un post
	 * @param $postId Id del post relacionado a los comentarios
	 */
	public function getComments(string $postId)
	{
		try {
		 	$apiInfo = new APIController();
		 	$commentList = $apiInfo->getCommentsUser($postId);
		 	return $commentList;
		} catch (Exception $e) {
		 	return 'Error al obtener los comentarios';	
		}
	}
}