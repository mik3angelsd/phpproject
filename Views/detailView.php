<?php 
	include('../Controllers/UsersController.php');

	$control = new UsersController();
	$detail = $control->getDetailUser($_POST['userId']);

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="https://cdn.tailwindcss.com?plugins=forms,typography,aspect-ratio,line-clamp"></script>
	<title>Detalles del usuario</title>
</head>
<body class="m-5">

	<div class="sm:grid sm:grid-cols-3">
		<form class="flex sm:justify-start my-2" action="../index.php">
			<input type="submit" class="p-2 text-white cursor-pointer rounded bg-gray-700 hover:bg-gray-400" value="Consultar otro usuario">
		</form>
		<form class="flex sm:justify-center my-2"  action="total.php" method="post">
			<input type="hidden" name="userId" value="<?= $_POST['userId'] ?>">
			<input type="submit" class="p-2 text-white cursor-pointer rounded bg-gray-700 hover:bg-gray-400" value="Ver tareas">
		</form>
		<form class="flex sm:justify-end my-2"  action="comments.php" method="post">
			<input type="hidden" name="userId" value="<?= $_POST['userId'] ?>">
			<input type="submit" class="p-2 text-white cursor-pointer rounded bg-gray-700 hover:bg-gray-400" value="Ver comentarios">
		</form>
	</div>
	<!--Información de cuenta-->
	<div class="rounded border-4 p-2 my-4">
		<h3 class="font-bold text-gray-600 text-2xl">Información del usuario</h3>
		<div class="sm:grid sm:grid-cols-3 my-4 gap-4">
			<p class="sm:col-span-2"><label class="font-semibold">Usuario: </label><?= $detail->username ?></p>
			<p><label class="font-semibold">Página web: </label><?= $detail->website ?></p>
			<p><label class="font-semibold">Nombre: </label><?= $detail->name ?></p>
			<p><label class="font-semibold">Correo: </label><?= $detail->email ?></p>
			<p><label class="font-semibold">Teléfono: </label><?= $detail->phone ?></p>
		</div>
	</div>

	<!--Información de la compañia-->
	<div class="rounded border-4 p-2 my-4">
		<h3 class="font-bold text-gray-600 text-2xl">Datos de empresa</h3>
		<div class="sm:grid sm:grid-cols-3 my-4 gap-4">
			<p><label class="font-semibold">Nombre de la compañía: </label><?= $detail->company->name ?></p>
			<p><label class="font-semibold">Frase: </label><?= $detail->company->catchPhrase ?></p>
			<p><label class="font-semibold">Objetivo: </label><?= $detail->company->bs ?></p>
		</div>
	</div>

	<!--Dirección-->
	<div class="rounded border-4 p-2 my-4">
		<h3 class="font-bold text-gray-600 text-2xl">Dirección</h3>
		<div class="sm:grid sm:grid-cols-4 my-4 gap-4">
			<p><label class="font-semibold">Calle: </label><?= $detail->address->street ?></p>
			<p><label class="font-semibold">Número: </label><?= $detail->address->suite ?></p>
			<p><label class="font-semibold">Ciudad: </label><?= $detail->address->city ?></p>
			<p><label class="font-semibold">Código postal: </label><?= $detail->address->zipcode ?></p>
			<p class="col-span-4">
				<iframe src="https://maps.google.com/?ll=<?= $detail->address->geo->lat?>,<?= $detail->address->geo->lng?>&z=15&output=embed" class="h-auto sm:h-64 w-full"></iframe>
			</p>

		</div>
	</div>
</body>
</html>