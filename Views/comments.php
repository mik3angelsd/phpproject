<?php
	include('../Controllers/UsersController.php');
	include('../Controllers/CommentsController.php');

	$control = new UsersController();
	$controlComments = new CommentsController();
	$postList = $control->getPosts($_POST['userId']);
	$commentList = (isset($_POST['postId']))? $controlComments->getComments($_POST['postId']) : [];

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="https://cdn.tailwindcss.com?plugins=forms,typography,aspect-ratio,line-clamp"></script>
	<title>Comentarios por el usuario</title>

</head>
<body class="p-5">
	<h2 class="text-lg font-bold">Comentarios realizados por el usuario</h2>
	<div class="grid grid-cols-2">
		<form class="flex justify-start" action="../index.php">
			<input type="submit" class="p-2 text-white cursor-pointer rounded bg-gray-700 hover:bg-gray-400" value="Consultar otro usuario">
		</form>
		<form class="flex justify-end"  action="detailView.php" method="post">
			<input type="hidden" name="userId" value="<?= $_POST['userId'] ?>">
			<input type="submit" class="p-2 text-white cursor-pointer rounded bg-gray-700 hover:bg-gray-400" value="Volver detalles de usuario">
		</form>
	</div>
	<div>
		<?php foreach($postList as $post): ?>

		<div class="rounded border-4 sm:grid sm:grid-cols-2 gap-4 my-4 p-2">
			<p><label class="font-semibold">Título: </label><?= $post->title ?></p>
			<form method="post">
				<input type="hidden" name="userId" value="<?= $_POST['userId'] ?>">
				<input type="hidden" name="postId" value="<?= $post->id ?>">
				<input class="p-2 rounded bg-gray-900 hover:bg-gray-500 text-white" type="submit" value="Ver comentarios">
			</form>
			
			<p class="col-span-2"><label class="font-semibold">Contenido: </label><?= $post->body ?></p>

			<!--Comentarios-->
			<?php if(!empty($commentList)): ?>
				<?php if($_POST['postId'] == $post->id): ?>
				<p class="text-lg font-semibold col-span-2">Comentarios realizados</p>
				<div class="my-4">
					<?php foreach ($commentList as $comment): ?>
						<div class="mt-2">
							<p class="my-1"><label class="font-semibold">Nombre: </label><?= $comment->name ?></p>
							<p class="my-1"><label class="font-semibold">Correo: </label><?= $comment->email ?></p>
							<p class="my-1"><label class="font-semibold">Contenido: </label><?= $comment->body ?></p>
						</div>
					<?php endforeach; ?>
				</div>
				<?php endif; ?>
			<?php endif; ?>
		</div>
		<?php endforeach; ?>
	</div>
</body>
</html>