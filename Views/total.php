<?php 
	include('../Controllers/UsersController.php');

	try {
		$control = new UsersController();
		$confirm = (isset($_POST['title']) && isset($_POST['completed']))? $control->sendTask($_POST['title'],$_POST['userId'],$_POST['completed']): null;
		$listTask = $control->getTasks($_POST['userId']);	
	} catch (Exception $e) {
		echo 'Error al cargar la información';
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="https://cdn.tailwindcss.com?plugins=forms,typography,aspect-ratio,line-clamp"></script>
	<title>Vista de tareas</title>
</head>
<body class="m-5">
	<h3 class="font-bold text-gray-600 text-2xl">Tareas del usuario</h3>

	<div class="grid grid-cols-2 my-2">
		<form class="flex justify-start" action="../index.php">
			<input type="submit" class="p-2 text-white cursor-pointer rounded bg-gray-700 hover:bg-gray-400" value="Consultar otro usuario">
		</form>
		<form class="flex justify-end"  action="detailView.php" method="post">
			<input type="hidden" name="userId" value="<?= $_POST['userId'] ?>">
			<input type="submit" class="p-2 text-white cursor-pointer rounded bg-gray-700 hover:bg-gray-400" value="Volver detalles de usuario">
		</form>
	</div>

	<div class="sm:grid sm:grid-cols-3 gap-4">
		<!--Listado de tareas-->
		<div class="col-span-2 rounded border-4 p-2">
			<?php foreach ($listTask as $task): ?>
				<p class="sm:grid-cols-3 sm:grid gap-4 my-4"><label class="font-semibold">Tarea: </label><?= $task->title?>
				<?php if($task->completed): ?>
				<label class="flex bg-green-500 rounded p-1 text-white">Completada</label>
				<?php else: ?>
				<label class="flex bg-red-500 rounded p-1 text-white">No completada</label>
				<?php endif; ?>
				</p>
			<?php endforeach; ?>
		</div>
		<!--Formulario-->
		<div class="rounded border-4 p-2">
			<p class="text-center my-4 text-xl">Registrar una nueva tarea</p>
			<?php if($confirm != null): ?>
				<?php if($confirm): ?>
					<p class="p-2 rounded bg-green-500 text-center text-white">Se ha registrado la tarea exitosamente.</p>
				<?php else: ?> 
					<p class="p-2 rounded bg-red-500 text-center text-white">No se registro la tarea.</p>
				<?php endif; ?> 
			<?php endif; ?> 
			<form class="p-2 grid grid-cols-3 gap-4" action="total.php" method="post">
				<input type="hidden" name="userId" value="<?= $_POST['userId'] ?>">
				<select class="rounded" name="completed" required>
					<option value="true">Completada</option>
					<option value="false">No completada</option>
				</select>
				<textarea class="col-span-3 rounded" required name="title" placeholder="Descripción de la tarea"></textarea>
				<input type="submit" class="rounded border bg-gray-700 hover:bg-gray-500 p-2 text-white cursor-pointer" name="register" value="Registrar tarea">
			</form>
		</div>
	</div>

</body>
</html>